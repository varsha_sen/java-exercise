package com.model;

public class Player {
	private String name;
	private String teamName;
	private int numOfMatches;
	public Player() {
		super();
	}
	public Player(String name, String teamName, int numOfMatches) {
		this.name = name;
		this.teamName = teamName;
		this.numOfMatches = numOfMatches;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public int getNumOfMatches() {
		return numOfMatches;
	}
	public void setNumOfMatches(int numOfMatches) {
		this.numOfMatches = numOfMatches;
	}
	@Override
	public String toString() {
		return "Employee [Name=" + name + 
			       ", TeamName=" + teamName + ", Number Of Matches=" + numOfMatches + "]"; 
	}
	

}
