package com.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * 
 *Program by using BufferedReader class to prompt a user to input his/her name.
 *
 */
public class BufferedReaderEx{    
public static void main(String args[])throws Exception{             
    InputStreamReader r=new InputStreamReader(System.in);    
    BufferedReader br=new BufferedReader(r);            
    System.out.println("Enter your name");    
    String name=br.readLine();    
    System.out.println("Hello "+name + "!");    
}    
}  