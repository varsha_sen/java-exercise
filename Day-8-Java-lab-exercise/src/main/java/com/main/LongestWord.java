package com.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * Program to find the longest word in a text file.
 *
 */
public class LongestWord {
	public static void main(String[] args) throws FileNotFoundException {
		new LongestWord().findLongestWords();
	}

	public String findLongestWords() throws FileNotFoundException {

		String longestWord = "";
		String current;
		try (Scanner scanner = new Scanner(new File("F:\\FileEx\\myfile.txt"))) {
			while (scanner.hasNext()) {
				current = scanner.next();
				if (current.length() > longestWord.length()) {
					longestWord = current;
				}

			}
		}

		System.out.println("\n" + longestWord + "\n");
		return longestWord;
		
	}
}