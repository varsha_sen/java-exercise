package com.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * Program to append text to an existing file.
 *
 */
public class AppendText {
	public static void main(String a[]) {
		StringBuilder stringBuilder = new StringBuilder();
		String strLine = "";
		try {
			String filename = "F:\\FileEx\\myfile.txt";
			FileWriter fileWriter = new FileWriter(filename, true);
			fileWriter.write("Topic One Lambda Expression\n");
			fileWriter.close();
			BufferedReader bufferedReader = new BufferedReader(new FileReader("F:\\FileEx\\myfile.txt"));
			while (strLine != null) {
				stringBuilder.append(strLine);
				stringBuilder.append(System.lineSeparator());
				strLine = bufferedReader.readLine();
				System.out.println(strLine);
			}
			bufferedReader.close();
		} catch (IOException ioe) {
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
}