package com.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/**
 * 
 * Program to write and read a plain text file. 
 *
 */
public class ReadText {
     public static void main(String a[]){
        StringBuilder stringBuilder = new StringBuilder();
        String strLine = "";
        try
          {
             String filename= "F:\\FileEx\\myfile.txt";
             FileWriter fileWriter = new FileWriter(filename,false); 
             fileWriter.write("Java8 study notes\n");
             fileWriter.close();
             BufferedReader bufferedReader = new BufferedReader(new FileReader("F:\\FileEx\\myfile.txt"));
             while (strLine != null)
             {
                stringBuilder.append(strLine);
                stringBuilder.append(System.lineSeparator());
                strLine = bufferedReader.readLine();
                System.out.println(strLine);
            }
             bufferedReader.close();                          
           }
           catch(IOException ioe)
           {
            System.err.println("IOException: " + ioe.getMessage());
           }
        }
  }