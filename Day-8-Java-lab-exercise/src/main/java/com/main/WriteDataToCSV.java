package com.main;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.model.Player;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

/**
 * 
 * @author sufi
 *
 */
public class WriteDataToCSV {

	public static void main(String[] args) {
		final String CSV_LOCATION = "F:\\FileEx\\player.csv ";
		try {

			FileWriter writer = new FileWriter(CSV_LOCATION);

			List<Player> playersList = new ArrayList();
			Player player = new Player("Sonu", "Mumbai", 15);
			Player player2 = new Player("Rahul", "Chennai", 14);
			Player player3 = new Player("Amit", "Delhi", 10);
			Player player4 = new Player("Raju", "Banglore", 20);

			playersList.add(player);
			playersList.add(player2);
			playersList.add(player3);
			playersList.add(player4);
			ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
			mappingStrategy.setType(Player.class);

			String[] columns = new String[] { "name", "teamName", "numOfMatches" };
			mappingStrategy.setColumnMapping(columns);

			StatefulBeanToCsvBuilder<Player> builder = new StatefulBeanToCsvBuilder(writer);
			StatefulBeanToCsv beanWriter = builder.withMappingStrategy(mappingStrategy).build();
			beanWriter.write(playersList); 
			  
            writer.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
