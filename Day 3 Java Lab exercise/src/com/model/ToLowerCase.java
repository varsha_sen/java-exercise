package com.model;

public class ToLowerCase {
	private String str;

	public ToLowerCase() {
		super();
	}

	public ToLowerCase(String str) {
		super();
		this.str = str;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

}
