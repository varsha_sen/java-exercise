package com.model;

public class SortArray {
	private int arr[];

	public SortArray() {
		super();
	}

	public SortArray(int[] arr) {
		super();
		this.arr = arr;
	}

	public int[] getArr() {
		return arr;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}

}
