package com.model;

public class CharReplace {
	private String str;

	public CharReplace() {
		super();
	}

	public CharReplace(String str) {
		super();
		this.str = str;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
}
