package com.main;

import com.model.ToLowerCase;
import com.service.ToLowerCaseService;

/**
 * 
 * Convert String character to Lower Case.
 *
 */
public class ToLowerCaseMain {

	public static void main(String[] args) {
		ToLowerCase tlcCase = new ToLowerCase("Hello! World, Good Moring");
		ToLowerCaseService.toLowerCase(tlcCase.getStr());

	}

}
