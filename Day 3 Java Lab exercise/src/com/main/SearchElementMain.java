package com.main;

import java.util.Scanner;

import com.service.SearchElementService;

/**
 * 
 * Search Element in array
 *
 */
public class SearchElementMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size;
		int search;
		System.out.println("Enter the number of elements:");
		size = scanner.nextInt();
		int[] arr = new int[size];

		System.out.println("Enter the elements");
		for (int i = 0; i < size; i++) {
			arr[i] = scanner.nextInt();
		}

		System.out.println("Enter the element to be seached");
		search = scanner.nextInt();
		SearchElementService.searchElement(arr, search);
		scanner.close();
		
	}

}
