package com.main;

import java.util.Scanner;
import com.service.ModifiyStringService;

/**
 * 
 * Modify a String.
 *
 */

public class ModifyMain {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		String s = scanner.nextLine();
		System.out.println(ModifiyStringService.display(s));
		
		scanner.close();
	}
}