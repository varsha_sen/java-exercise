package com.main;

import com.model.SortArray;
import com.service.SortArrayService;

/**
 * 
 * Sort Array elements in ascending order
 *
 */
public class SortArrayMain {

	public static void main(String[] args) {
		int a[] = {1, 5, 8, 2, 3, 6, 9,10, 4};
		SortArray sortArray = new SortArray(a);
		SortArrayService.arraySort(sortArray.getArr());
	}

}
