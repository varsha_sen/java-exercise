package com.main;

import java.util.Scanner;

import com.service.StringReverseService;

/**
 * 
 * String Reverse Program
 *
 */
public class StringReverseMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String s1 = scanner.nextLine();
		StringReverseService.reShape(s1);
		
		scanner.close();
	}

}
