package com.main;

import java.util.Scanner;

import com.service.PalindromeService;

/**
 * 
 * Palindrome Program
 *
 */
public class PalindromeMain {

	// Driver code
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter String : ");
		String str = scanner.nextLine();

		if (PalindromeService.isPalindrome(str)) {
			System.out.print("Yes");
		}
		else {
			System.out.print("No");
		}
		scanner.close();
	}
}