package com.main;
import java.util.Scanner;
/**
 * 
 * SubString Program
 *
 */

public class SubString {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter String : ");
		String str = scan.next();
		System.out.println("Enter Start value : ");
		int start = scan.nextInt();
		System.out.println("Enter Last value : ");
		int end = scan.nextInt();
		scan.close();

		System.out.println(str.substring(start, end));
	}
}