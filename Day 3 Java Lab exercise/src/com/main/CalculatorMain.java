package com.main;

import com.hcl.Calculator;

/**
 * 
 * Call add method from another package.
 *
 */
public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		calculator.add(85, 79);

	}

}
