package com.main;

import java.util.Scanner;

import com.service.PangramService;

/**
 * 
 * Check String are Pangram or not.
 *
 */
public class PangramMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter String : ");
		String str = scanner.nextLine();

		if (PangramService.checkPangram(str) == true) {
			System.out.print(str + " is a pangram.");
		}
		else {
			System.out.print(str + " is not a pangram.");
		}
		scanner.close();
	}

}
