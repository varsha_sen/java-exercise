package com.main;

import com.model.CharReplace;
import com.service.CharReplaceService;

/**
 * 
 * Character Replace in a String.
 *
 */
public class CharReplaceMain {

	public static void main(String[] args) {
		CharReplace charReplace = new CharReplace("World Wide");
		CharReplaceService.replaceChar(charReplace.getStr());
		

	}

}
