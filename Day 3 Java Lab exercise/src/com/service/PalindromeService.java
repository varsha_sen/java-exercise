package com.service;

public class PalindromeService {

	public static boolean isPalindrome(String str) // Method to Check String is palindrome or not
	{

		int i = 0, j = str.length() - 1;

		while (i < j) {

			if (str.charAt(i) != str.charAt(j)) {
				return false;
			}

			i++;
			j--;
		}

		return true;
	}

}
