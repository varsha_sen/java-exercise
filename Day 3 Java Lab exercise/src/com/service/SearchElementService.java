package com.service;

public class SearchElementService {
	public static void searchElement(int[] arr,int search) {
		int flag = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == search) {
				System.out.println("Element " + search + " found at " + i + " position");
				flag = 1;
				break;
			}
		}
		if (flag == 0) {
			System.out.println("Element " + search + " not found");
		}
	}

}
