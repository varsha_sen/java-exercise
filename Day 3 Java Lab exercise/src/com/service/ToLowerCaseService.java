package com.service;

public class ToLowerCaseService {
	public static void toLowerCase(String str) {
		String lowerStr = str.toLowerCase();

        // Display the two strings for comparison.
        System.out.println("Original String: " + str);
        System.out.println("String in lowercase: " + lowerStr);
	}

}
