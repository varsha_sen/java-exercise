package com.service;

public class CharReplaceService {
	public static void replaceChar(String str) {
		String new_str = str.replace('d', 'h');

        // Display the two strings for comparison.
        System.out.println("Original String: " + str);
        System.out.println("String in after Replace : " +new_str);
	}
}
