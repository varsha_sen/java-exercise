package com.service;

public class Number implements Runnable {
	public void myRunable(int num1, int num2) {
		if ((num1 % num2) == 0) {
            System.out.println(num1 + " is a multiple of " + num2);
        } else {
            System.out.println(num1 + " is not a multiple of " + num2);
        }
		
	}

	@Override
	public void run() {
		
	}

}
