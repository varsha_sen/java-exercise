package com.service;

public class DemoThread2 extends Thread {
	public String thread1;

	public DemoThread2(String thread1) {
		this.thread1 = thread1;
		System.out.println("Create " + thread1);
	}

	@Override
	public void run() {
		System.out.println("running child Thread in loop : ");

		try {
			for (int i = 0; i <= 10; i++) {
				System.out.println(i);
				Thread.sleep(2000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
