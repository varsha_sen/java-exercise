package com.main;

import com.service.Number;

public class NumberMain {

	public static void main(String[] args) {
		Thread t1 = new Thread(new Number());
		Thread t2 = new Thread(new Number());
		Thread t3 = new Thread(new Number());
		t1.start();
		t2.start();
		t3.start();
	}

}
