package com.main;

import com.service.DemoThread2;
/**
 * 
 * Rewrite the earlier program so that, now the class DemoThread1 instead of implementing 
 * from Runnable interface, will now extend from Thread class.
 *
 */

public class DemoThread2Main {

	public static void main(String[] args) {
		DemoThread2 demoThread1 = new DemoThread2("FirstThread");
		DemoThread2 demoThread2 = new DemoThread2("SecondThread");
		DemoThread2 demoThread3 = new DemoThread2("ThirdThread");
		demoThread1.start();
		demoThread2.start();
		demoThread3.start();

	}

}
