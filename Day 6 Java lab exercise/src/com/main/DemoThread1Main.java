package com.main;

import com.service.DemoThread1;
/**
 * 
 * Write a program to create a class DemoThread1 implementing Runnable interface.
 *
 */
public class DemoThread1Main {

	public static void main(String[] args) {
		DemoThread1 demoThread1 = new DemoThread1("FirstThread");
		DemoThread1 demoThread2 = new DemoThread1("SecondThread");
		DemoThread1 demoThread3 = new DemoThread1("ThirdThread");
		Thread thread1 = new Thread(demoThread1);
		Thread thread2 = new Thread(demoThread2);
		Thread thread3 = new Thread(demoThread3);
		thread1.start();
		thread2.start();
		thread3.start();
	}

}
