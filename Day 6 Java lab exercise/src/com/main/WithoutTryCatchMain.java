package com.main;

import com.service.WithoutTryCatchSleep;
/**
 * 
 * In the previous program remove the try{}catch(){} block 
 * surrounding the sleep method and try to execute the code.
 *
 */
public class WithoutTryCatchMain {

	public static void main(String[] args) {
		WithoutTryCatchSleep withoutTryCatchSleep = new WithoutTryCatchSleep("t1");
        withoutTryCatchSleep.start();
        withoutTryCatchSleep.setThreadName("MyThread");

	}

}
