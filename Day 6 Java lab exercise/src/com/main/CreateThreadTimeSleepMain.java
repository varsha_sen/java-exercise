package com.main;

import com.service.CreateThreadTimeSleep;

/**
 * 
 * Write a program to assign the current thread to t1. Change the name of the
 * thread to MyThread. Display the changed name of the thread. Also it should
 * display the current time.
 *
 */
public class CreateThreadTimeSleepMain {
	public static void main(String args[]) {
		CreateThreadTimeSleep createThreadTimeSleep = new CreateThreadTimeSleep("t1");
		createThreadTimeSleep.start();
		createThreadTimeSleep.setThreadName("MyThread");
	}
}