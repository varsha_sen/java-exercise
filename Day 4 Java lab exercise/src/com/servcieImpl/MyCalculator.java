package com.servcieImpl;

import com.service.AdvancedArithmetic;

public class MyCalculator implements AdvancedArithmetic {

	@Override
	public int divisor_Sum(int n) {
		int sum = 0;
		int sqrt = (int) Math.sqrt(n);
		for (int i = 1; i <= sqrt; i++) {
			if (n % i == 0) {
				sum += i + n / i;
			}
		}

		if (sqrt * sqrt == n) {
			sum -= sqrt;
		}
		return sum;
	}

}
