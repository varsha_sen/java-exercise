package com.main;

import com.servcieImpl.Aa;
import com.servcieImpl.Bb;
import com.servcieImpl.Cc;

public class OOPExercises3 {
	static int a = 555;

	public static void main(String[] args) {
		Aa objAa = new Aa();
		Bb objBb1 = new Bb();
		Aa objBb2 = new Bb();
		Cc objCc1 = new Cc();
		Bb objCc2 = new Cc();
		Aa objCc3 = new Cc();
		objAa.display();
		objBb1.display();
		objBb2.display();
		objCc1.display();
		objCc2.display();
		objCc3.display();
	}
}
