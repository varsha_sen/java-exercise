package com.main;

import java.util.Scanner;

import com.area.Circle;
import com.area.Rectangle;
import com.area.Square;

/**
 * 
 * Find Area of Circle, Square and Rectangle.
 *
 */
public class AreaMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.printf("Enter Radius value : ");
		Integer radius = scanner.nextInt();
		Circle circle = new Circle("Circle", radius);
		System.out.print("Area of Circle is : ");
		System.out.printf("%.2f", circle.calculateArea());
		System.out.println();
		System.out.printf("Enter side value : ");
		Integer side = scanner.nextInt();
		Square squre = new Square("Square", side);
		System.out.print("Area of Squre is : ");
		System.out.printf("%.2f", squre.calculateArea());
		System.out.println();
		System.out.printf("Enter length & width value : ");
		Integer length = scanner.nextInt();
		Integer width = scanner.nextInt();
		Rectangle rectangle = new Rectangle("Rectangle", length, width);
		System.out.print("Area of Squre is : ");
		System.out.printf("%.2f", rectangle.calculateArea());
		scanner.close();

	}

}
