package com.main;

import java.util.Scanner;

import com.servcieImpl.MyCalculator;

/**
 * 
 * Interface example to calculate the integer as input and return the sum of all
 * its divisors.
 *
 */
public class MyCalculaterMain {

	public static void main(String[] args) {
		MyCalculator my_calculator = new MyCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter an integer value : ");
		int n = sc.nextInt();
		System.out.print(my_calculator.divisor_Sum(n) + "\n");
		sc.close();
	}

}
