package com.main;

import java.util.Scanner;

import com.model.Card;
import com.model.MembershipCard;
import com.model.PaybackCard;

/**
 * 
 * Inheritance Example
 *
 */
public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Card card = new Card();
		System.out.println("Enter select value");
		System.out.println("Select the Card");
		System.out.println("1. MembershipCard");
		System.out.println("2. PaybackCard");
		int select = scanner.nextInt();
		if (select == 1) {
			System.out.println("Enter Card Details : ");
			String holderName = scanner.next();
			String cardNumber = scanner.next();
			String expiryDate = scanner.next();
			Integer rating = scanner.nextInt();
			new MembershipCard(holderName, cardNumber, expiryDate, rating);
			System.out.println("Card Details" + holderName + "|" + cardNumber + "|" + expiryDate);
			System.out.println("Rating :" + rating);
		} else if (select == 2) {
			System.out.println("Anandhi's Payback Card Details: : ");
			String holderName = scanner.next();
			String cardNumber = scanner.next();
			String expiryDate = scanner.next();
			Integer pointsEarned = scanner.nextInt();
			Double totalAmount = scanner.nextDouble();
			new PaybackCard(holderName, cardNumber, expiryDate, pointsEarned, totalAmount);
			System.out.println(holderName + "Payback Card Details: ");
			System.out.println("Card Number : " + cardNumber);
			System.out.println("Point Earned : " + pointsEarned);
			System.out.println("Total Amount : " + totalAmount);
		} else {
			System.out.println("select valid option.");
		}
		scanner.close();
	}

}
