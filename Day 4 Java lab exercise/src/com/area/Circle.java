package com.area;

public class Circle extends Shape {
	private Integer radius;

	public Circle(String name, Integer radius) {
		super(name);
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	@Override
	public Float calculateArea() {
		Float area = (float) (3.14 * radius * radius);
		return area;
	}

}
