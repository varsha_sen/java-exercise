package com.area;

public class Rectangle extends Shape{
	private Integer length;
	private Integer width;
	public Rectangle(String name, Integer length, Integer width) {
		super(name);
		this.length = length;
		this.width = width;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getWidth() {
		return width;
	}
	public void setBreadth(Integer width) {
		this.width = width;
	}
	@Override
	public Float calculateArea() {
		Float area = (float) (length*width);
		return area;
	}
	
}
