package com.model;

public class MembershipCard extends Card {
	private Integer rating;

	public MembershipCard() {
		super();
	}

	public MembershipCard(String holderName,String cardNumber,String expiryDate, Integer rating) {
		super.holderName = holderName;
		super.cardNumber = cardNumber;
		super.expiryDate = expiryDate;
		this.rating = rating;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
}
