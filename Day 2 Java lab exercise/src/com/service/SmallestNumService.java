package com.service;

public class SmallestNumService {
	public int findSmallest(int n1, int n2, int n3) {
		int temp = 0;
		
		if((n1<n2) && (n1<n3)) {
			temp = n1;
		}
		else if(n2<n1 && n2<n3) {
			temp = n2;
		}
		else {
			temp = n3;
		}
		return temp;
	}

}
