package com.service;

public class AdditionService {
	public static void addition(int... numbers) {
		int sum = 0;
        for(int num:numbers)
        {
            if(sum != 0){
                System.out.print("+");
            }
            sum += num;
            System.out.print(num);
        }
        System.out.println("="+sum);
	}

}
