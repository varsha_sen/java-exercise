package com.main;

import com.model.Room;

/**
 * 
 * Create Room class with attributes.
 *
 */
public class RoomMain {

	public static void main(String[] args) {
		float length = 20;
		float width = 20;
		float area = length * width;
		
		Room room = new Room(101,"Study Room",area, 1);
		room.displayData();

	}

}
