package com.main;

import java.util.Scanner;

import com.model.MiddleChar;
import com.service.MiddleCharService;

/**
 * 
 * Find middle character of a string
 *
 */
public class MiddleCharMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input a string: ");
		String str = scanner.nextLine();

		MiddleChar middleChar = new MiddleChar(str);
		MiddleCharService middleCharService = new MiddleCharService();
		System.out.println("Middle Character is : " + middleCharService.middel(middleChar.getS1()));
		scanner.close();
	}

}
