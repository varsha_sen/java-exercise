package com.main;

import java.util.Scanner;

import com.model.CountVowel;
import com.service.CountVowelService;

/**
 * 
 * Count Vowel of a String.
 *
 */
public class CountVowelMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a String : ");
		String str = scanner.nextLine();
		
		CountVowel countVowel = new CountVowel(str);
		CountVowelService countVowelService = new CountVowelService();
		System.out.println("Vowel are : "+countVowelService.countV(countVowel.getStr()) );
		scanner.close();

	}

}
