package com.main;
/**
 * 
 * print "Hello I am a motorcycle, I am a cycle with an engine".
 * My ancestor is a cycle who is a vehicle with pedals
 * 
 *
 */
class Cycle {
	String define_me() {
		return "a vehicle with pedals.";
	}
}

class Bike extends Cycle {
	String define_me() {
		return "a cycle with an engine.";
	}

	Bike() {
		String temp = define_me();
		System.out.println("Hello I am a motorcycle I am " + temp);
		System.out.println("My ancestor is a cycle who is " + super.define_me());
	}
}

public class InheritenceExample {
	public static void main(String[] args) {
		Bike b = new Bike();
	}
}
