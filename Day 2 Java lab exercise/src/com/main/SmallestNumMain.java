package com.main;

import java.util.Scanner;

import com.model.SmallestNum;
import com.service.SmallestNumService;

/**
 * 
 * Find the smallest number among three numbers.
 *
 */
public class SmallestNumMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter First Number : ");
		int n1 = scanner.nextInt();

		System.out.println("Enter Second Number : ");
		int n2 = scanner.nextInt();

		System.out.println("Enter Third Number : ");
		int n3 = scanner.nextInt();

		SmallestNum smallestNum = new SmallestNum(n1, n2, n3);
		SmallestNumService smallestNumService = new SmallestNumService();

		System.out.println("Smallest number is : "
				+ smallestNumService.findSmallest(smallestNum.getN1(), smallestNum.getN2(), smallestNum.getN3()));

		scanner.close();

	}

}
