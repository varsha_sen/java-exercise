package com.main;

/**
 * 
 * Inheritance Example
 *
 */
class A{                          // Super Class
	String str;                   // class variable
	public A() {                  // A Constructor
		System.out.println("Super Class");
	}
	public void show() {         // method
		System.out.println("Good Morning");
	}
}
class B extends A{              // Derived Class
	public B() {                // B Constructor
		System.out.println("Derived Class");
		str = "Hello World";
		
		System.out.println(str);
	}
	public void show1() {       // method
		System.out.println("Have a nice day!");
	}
}

public class ABMain {              // main class

	public static void main(String[] args) {       // main method
		B b = new B();
		
		b.show();
		b.show1();
	}

}
