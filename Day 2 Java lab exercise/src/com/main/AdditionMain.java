package com.main;

import com.model.Addition;
import com.service.AdditionService;

/**
 * 
 * Prints the sum of the numbers passed to the function addition.
 *
 */
public class AdditionMain {

	public static void main(String[] args) {
		int arr[] = {1, 2, 3, 4, 5, 6};
		Addition addition =  new Addition(arr);
		AdditionService.addition(addition.getNumbers());
		
	}

}
