package com.model;

public class Addition {
	private int[] numbers;

	public Addition() {
		super();
	}

	public Addition(int[] numbers) {
		super();
		this.numbers = numbers;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}

}
