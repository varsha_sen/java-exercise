package com.model;

public class CountVowel {
	private String str;

	public CountVowel() {
		super();
	}

	public CountVowel(String str) {
		super();
		this.str = str;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

}
