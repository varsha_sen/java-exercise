package com.model;

public class Room {
	private int roomno;
	private String roomtype;
	private float roomarea;
	private int acmachine;
	public Room() {
		super();
	}
	public Room(int roomno, String roomtype, float roomarea, int acmachine) {
		this.roomno = roomno;
		this.roomtype = roomtype;
		this.roomarea = roomarea;
		this.acmachine = acmachine;
	}
	public int getRoomno() {
		return roomno;
	}
	public void setRoomno(int roomno) {
		this.roomno = roomno;
	}
	public String getRoomtype() {
		return roomtype;
	}
	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
	public float getRoomarea() {
		return roomarea;
	}
	public void setRoomarea(float roomarea) {
		this.roomarea = roomarea;
	}
	public int getAcmachine() {
		return acmachine;
	}
	public void setAcmachine(int acmachine) {
		this.acmachine = acmachine;
	}
	public void displayData() {
		System.out.println("roomno : "+getRoomno());
		System.out.println("roomtype : "+getRoomtype());
		System.out.println("roomarea : "+getRoomarea());
		System.out.println("ACmachine : "+getAcmachine());
	}

}
