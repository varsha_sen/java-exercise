package com.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserMainCode5 {
	public static void getMonthDifference(String str1, String str2) throws ParseException {
		int res = 0;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			Calendar c = Calendar.getInstance();
			Date d1 = sdf.parse(str1);
			c.setTime(d1);
			int mon1 = c.get(Calendar.MONTH);
			int year1 = c.get(Calendar.YEAR);
			Date d2 = sdf.parse(str2);
			c.setTime(d2);
			int mon2 = c.get(Calendar.MONTH);
			int year2 = c.get(Calendar.YEAR);
			if (year1 >= year2) {
				res = Math.abs((year1 - year2) * 12 + (mon1 - mon2));
				System.out.println(res);
			} else {
				res = Math.abs((year2 - year1) * 12 + (mon2 - mon1));
				System.out.println(res);
			}

	}
}