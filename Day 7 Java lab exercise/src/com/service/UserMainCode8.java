package com.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserMainCode8 {
	public static String convertDateFormat(String dateValue)throws ParseException{
		SimpleDateFormat sdfSource = new SimpleDateFormat("dd/MM/yyyy");
	      
	      //parse the string into Date object
	      Date date = sdfSource.parse(dateValue);
	      
	      //create SimpleDateFormat object with desired date format
	      SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yy");
	      
	      //parse the date into another format
	      dateValue = sdfDestination.format(date);
	      
	      return dateValue;
	}

}
