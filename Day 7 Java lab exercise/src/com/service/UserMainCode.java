package com.service;

public class UserMainCode {
	public static int getNumberOfDays(int year, int monthCode) {
		int number_Of_Days = 0;
		String MonthOfName = "Unknown";

		switch (monthCode) {
		case 0:
			MonthOfName = "January";
			number_Of_Days = 31;
			break;
		case 1:
			MonthOfName = "February";
			if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
				number_Of_Days = 29;
			} else {
				number_Of_Days = 28;
			}
			break;
		case 2:
			MonthOfName = "March";
			number_Of_Days = 31;
			break;
		case 3:
			MonthOfName = "April";
			number_Of_Days = 30;
			break;
		case 4:
			MonthOfName = "May";
			number_Of_Days = 31;
			break;
		case 5:
			MonthOfName = "June";
			number_Of_Days = 30;
			break;
		case 6:
			MonthOfName = "July";
			number_Of_Days = 31;
			break;
		case 7:
			MonthOfName = "August";
			number_Of_Days = 31;
			break;
		case 8:
			MonthOfName = "September";
			number_Of_Days = 30;
			break;
		case 9:
			MonthOfName = "October";
			number_Of_Days = 31;
			break;
		case 10:
			MonthOfName = "November";
			number_Of_Days = 30;
			break;
		case 11:
			MonthOfName = "December";
			number_Of_Days = 31;
		}
		return number_Of_Days;
	}

}
