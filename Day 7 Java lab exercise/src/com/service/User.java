package com.service;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class User {
	public static LinkedHashMap<String, String> obtainDesignation(LinkedHashMap<String, String> h1, String n) {
		LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
		Iterator<String> iterator = h1.keySet().iterator();
		while (iterator.hasNext()) {
			String str2 = iterator.next();
			String str3 = h1.get(str2);
			if (str3.equals(n))
				linkedHashMap.put(str2, str3);
		}
		return linkedHashMap;
	}
}
