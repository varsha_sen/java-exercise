package com.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserMainCode4 {
	public static String findOldDate(String str1, String str2) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
			try {
				Date date1 = simpleDateFormat.parse(str1);
				Date date2 = simpleDateFormat.parse(str2);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date1);
				long y = calendar.getTimeInMillis();
				calendar.setTime(date2);
				long y1 = calendar.getTimeInMillis();
				String str3 = simpleDateFormat2.format(date1);
				String str4 = simpleDateFormat2.format(date2);
				if (y < y1) {
					return str3;
				}
				else {
					return str4;
				}
			} catch (ParseException e) {
				return "invalid";
			}
	}
}