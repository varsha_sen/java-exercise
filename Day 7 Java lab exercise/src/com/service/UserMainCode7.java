package com.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserMainCode7 {
	public static int getDateDifference(String s1, String s2) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(false);
		Calendar calendar = Calendar.getInstance();
		Date date1 = sdf.parse(s1);
		calendar.setTime(date1);
		long t1 = calendar.getTimeInMillis();
		Date date2 = sdf.parse(s2);
		calendar.setTime(date2);
		long t2 = calendar.getTimeInMillis();
		long t = t2 - t1;
		int result = (int) (t / (1000 * 24 * 60 * 60));
		return result;

	}

}
