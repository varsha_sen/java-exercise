package com.service;

import java.util.StringTokenizer;

public class UserMainCode2 {
	public static int checkCharacters(String input) {
		StringTokenizer t = new StringTokenizer(input, " ");
		String s = t.nextToken();
		String s1 = " ";
		while (t.hasMoreTokens()) {
			s1 = t.nextToken();
		}
		if (s.charAt(0) == s1.charAt(s1.length() - 1))
			return 1;
		else
			return 0;
	}
}
