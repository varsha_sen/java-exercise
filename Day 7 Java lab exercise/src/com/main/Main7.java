package com.main;

import java.util.Scanner;

import com.service.UserMainCode6;

/**
 * 
 * Program to read a string and validate the IP address.
 *
 */
public class Main7 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter IPAddress : ");
		String ipAddress = scanner.nextLine();
		boolean b = UserMainCode6.ipValidator(ipAddress);
		if (b == true) {
			System.out.println("Valid");
		} else {
			System.out.println("Invalid");
		}
		scanner.close();
	}

}
