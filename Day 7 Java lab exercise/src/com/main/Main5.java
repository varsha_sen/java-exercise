package com.main;

import java.util.Scanner;

import com.service.UserMainCode4;
/**
 * 
 * Compare the two dates and return the older date in 'MM/DD/YYYY' format.
 *
 */
public class Main5 {

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter first String : ");
		String str1 = scanner.next();
		System.out.println("Enter Second String : ");
		String str2 = scanner.next();
		System.out.println(UserMainCode4.findOldDate(str1, str2));

		scanner.close();
	}

}
