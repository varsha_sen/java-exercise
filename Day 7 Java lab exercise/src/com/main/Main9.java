package com.main;

import java.text.ParseException;
import java.util.Scanner;

import com.service.UserMainCode8;

public class Main9 {
	public static void main(String args[])throws ParseException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Date in the format dd/mm/yy : ");
		String string = scanner.nextLine();

		System.out.println(UserMainCode8.convertDateFormat(string));
		scanner.close();

	}
}
