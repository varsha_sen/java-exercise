package com.main;
import java.util.Scanner;

import com.model.FindDay;
import com.service.FindDayService;
/**
 * 
 * Program to find out total number of days in the given month for the given year.
 *
 */
public class FindDayMain {

    
  public static void main(String[] strings) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Input a month number: ");
        int month = scanner.nextInt();

        System.out.print("Input a year: ");
        int year = scanner.nextInt();
        
        FindDay findDay = new FindDay(month, year);
        FindDayService.findDayFun(findDay.getMonth(), findDay.getYear());
        scanner.close();
        
    }
}
