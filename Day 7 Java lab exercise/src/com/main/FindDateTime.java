package com.main;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
/**
 * 
 * Print Current Date and Time using Java8 DateTime API.
 *
 */
public class FindDateTime{
	public static void main(String[] args) {
		DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtFormatter.format(now));
	}
}