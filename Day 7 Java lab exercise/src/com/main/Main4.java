package com.main;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;

import com.service.User;
/**
 * 
 * A Company wants to obtain employees of a particular designation. 
 * You have been assigned as the programmer to build this package. 
 * You would like to showcase your skills by creating a quick prototype.
 *
 */
public class Main4 {

	public static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		System.out.println("Enter the size of Hash Map : ");
		int size = Integer.parseInt(scanner.nextLine());
		LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
		System.out.println("Enter the Employee name and Designation : ");
		for (int i = 0; i < size; i++) {
			String empName = scanner.nextLine();
			String Designation = scanner.nextLine();
			linkedHashMap.put(empName, Designation);
		}
		String n = scanner.nextLine();
		LinkedHashMap<String, String> linkedHashMap2 = new LinkedHashMap<String, String>();
		linkedHashMap2 = User.obtainDesignation(linkedHashMap, n);
		Iterator<String> iterator = linkedHashMap2.keySet().iterator();

		while (iterator.hasNext()) {
			String str2 = iterator.next();
			System.out.println(str2);
		}
	}
}
