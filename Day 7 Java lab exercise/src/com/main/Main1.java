package com.main;

import java.util.Scanner;

import com.service.UserMainCode;
/**
 * 
 * The method returns an integer corresponding to the number of days in the month. 
 *
 */

public class Main1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

        System.out.print("Input a year: ");
        int year = scanner.nextInt();

        System.out.print("Input a month number: ");
        int monthCode = scanner.nextInt();
        
        System.out.println(UserMainCode.getNumberOfDays(year, monthCode));
        scanner.close();

	}

}
