package com.main;
import java.util.*;
/**
 * 
 * Get the maximum value of the year, month, week, date 
 * from the current date of a default calendar.
 *
 */
public class MaxValue {
 public static void main(String[] args)
    {
        Calendar calendar = Calendar.getInstance();
		System.out.println("\nCurrent Date and Time: " + calendar.getTime());		
		int maxYear = calendar.getActualMaximum(Calendar.YEAR);
		int maxMonth = calendar.getActualMaximum(Calendar.MONTH);
		int maxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
		int maxDate = calendar.getActualMaximum(Calendar.DATE);
		
		System.out.println("Actual Maximum Year: "+maxYear);
		System.out.println("Actual Maximum Month: "+maxMonth);
		System.out.println("Actual Maximum Week of Month: "+maxWeek);
		System.out.println("Actual Maximum Date: "+maxDate+"\n");
		System.out.println();		
	  }
}