package com.main;

import java.text.ParseException;
import java.util.Scanner;

import com.service.UserMainCode7;
/**
 * 
 * Find difference between two dates in days. 
 *
 */
public class Main8 {

	public static void main(String[] args) throws ParseException {
		Scanner scanner = new Scanner(System.in);
		String str1 = scanner.nextLine();
		String str2 = scanner.nextLine();

		System.out.println(UserMainCode7.getDateDifference(str1, str2));
		scanner.close();

	}

}
