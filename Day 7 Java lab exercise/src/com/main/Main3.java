package com.main;

import java.util.ArrayList;
import java.util.Scanner;

import com.service.UserMainCode3;

/**
 * 
 * Write a code to read array lists of size 5 each as input and to merge the two
 * arrayLists, sort the merged arraylist in ascending order and fetch the
 * elements at 2nd, 6th and 8th index into a new arrayList and return the final
 * ArrayList.
 *
 */
public class Main3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ArrayList<Integer> arrayList1 = new ArrayList<Integer>();
		ArrayList<Integer> arrayList2 = new ArrayList<Integer>();
		ArrayList<Integer> ans = new ArrayList<Integer>();
		System.out.println("Enter First ArrayList Elementes : ");
		for (int i = 0; i < 5; i++)
			arrayList1.add(scanner.nextInt());
		System.out.println("Enter Second ArrayList Elementes : ");
		for (int j = 0; j < 5; j++)
			arrayList2.add(scanner.nextInt());
		ans = UserMainCode3.sortMergedArrayList(arrayList1, arrayList2);
		for (int k = 0; k < 3; k++)
			System.out.println(ans.get(k));

		scanner.close();
	}
}