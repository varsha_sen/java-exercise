package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

import com.service.UserMainCode5;
/**
 * 
 * Find the difference between two dates in months.
 *
 */
public class Main6 {

	public static void main(String[] args) throws ParseException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter First Date : ");
        String s1 = br.readLine();
        System.out.println("Enter Second Date : ");
        String s2 = br.readLine();
        
           UserMainCode5.getMonthDifference(s1,s2);

	}

}
