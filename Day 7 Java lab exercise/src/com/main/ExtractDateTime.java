package com.main;

import java.util.*;
import java.text.*;

public class ExtractDateTime {
	public static void main(String[] args) {
		try {
			String str = "2016-07-14 09:00:02";
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
			String newstr = new SimpleDateFormat("MM/dd/yyyy, H:mm:ss").format(date);
			System.out.println("String Date Time :" + str);
			System.out.println("\n" + newstr + "\n");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}