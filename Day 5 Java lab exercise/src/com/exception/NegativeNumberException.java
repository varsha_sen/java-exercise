package com.exception;

public class NegativeNumberException extends Exception {
	public NegativeNumberException(String s) {
		super(s);
	}

}
