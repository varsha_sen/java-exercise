package com.model;

public class IPL {
	private String playerName;
	private int playerAge;
	public IPL() {
		super();
	}
	public IPL(String playerName, int playerAge) {
		super();
		this.playerName = playerName;
		this.playerAge = playerAge;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public int getPlayerAge() {
		return playerAge;
	}
	public void setPlayerAge(int playerAge) {
		this.playerAge = playerAge;
	}

}
