package com.service;

import com.exception.InvalidAgeException;

public class IPLService {
	public void validate(String name, int age) throws InvalidAgeException {
		if(age < 19) {
			throw new InvalidAgeException("Player " +name+ " is not eligible for IPL");
		}
		else {
			System.out.println("Player "+name+" Welcom to IPL");
		}
	}

}
