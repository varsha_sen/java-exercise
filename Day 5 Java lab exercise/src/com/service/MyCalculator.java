package com.service;

import com.exception.NegativeNumberException;
import com.exception.NumberException;
import com.exception.ZeroNumberException;

public class MyCalculator {
	public long power(int num1, int num2) throws NegativeNumberException, ZeroNumberException, NumberException {
		long result;
		if(num1 < 0 && num2 < 0) {
			throw new NegativeNumberException("num1 and num2 should not be negative");
		}
		else if(num1 == 0 && num2 == 0) {
			throw new ZeroNumberException("num1 and num2 should not be Zero");
		}
		else if(num1<= 0 && num2 <= 0) {
			throw new NumberException("num1 and num2 may be Negative or Zero");
		}
		else 
		result = (int)Math.pow((double)num1,(double)num2);
		return result;
	}

}
