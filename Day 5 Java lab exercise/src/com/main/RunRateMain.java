package com.main;

import java.io.IOException;
import java.util.Scanner;

import com.service.RunRateService;

public class RunRateMain {
	
	
	public static void main(String[] args) throws IOException {
		int runs;
		int balls;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Runs Scored: ");
		runs = scanner.nextInt();
		System.out.println("Enter Balls Delivered: ");
		balls = scanner.nextInt();
		
		RunRateService rateService = new RunRateService();
		try {
			rateService.findRunRate(runs, balls);

		} catch (NumberFormatException e) {
			System.out.println("Error Code: " + e);
		}
		scanner.close();
	}
}
