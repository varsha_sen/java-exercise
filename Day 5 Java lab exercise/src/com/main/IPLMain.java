package com.main;

import java.util.Scanner;

import com.model.IPL;
import com.service.IPLService;

public class IPLMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Player Name: ");
		String name = scanner.nextLine();
		System.out.print("Enter Player Age: ");
		int age = scanner.nextInt();
		IPL ipl = new IPL(name,age);
		IPLService iplService = new IPLService();
		try {
			iplService.validate(ipl.getPlayerName(), ipl.getPlayerAge());
		}
		catch (Exception e) {
			System.out.println("Exception occured : "+e);
		}
		scanner.close();
	}

}
