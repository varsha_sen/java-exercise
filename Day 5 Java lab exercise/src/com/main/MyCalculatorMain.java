package com.main;

import java.util.Scanner;

import com.service.MyCalculator;

public class MyCalculatorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter vaule of n1 : ");
		int n1 = scanner.nextInt();
		System.out.print("Enter value of n2 : ");
		int n2 = scanner.nextInt();
		MyCalculator myCalculator = new MyCalculator();
		try {
			System.out.println("Result : " + myCalculator.power(n1, n2));
		}
		catch (Exception e) {
			System.out.println("Exception occured : "+e);
		}
		scanner.close();

	}

}
