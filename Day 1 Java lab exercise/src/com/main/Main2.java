package com.main;
/**
 * Finds the longest word from a sentence.
 */
import java.util.Scanner;

import com.service.UserMainCode2;

public class Main2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a String : ");
		String str1 = scanner.nextLine();

		System.out.println(UserMainCode2.getLargestWord(str1));

		scanner.close();
	}

}