package com.main;
/**
 * Add Odd numbers of given integer.
 */
import java.util.Scanner;

import com.service.AddOddService;
public class AddOddMain {
  
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter the value of num : ");
    int num = scanner.nextInt();
    AddOddService addOddService = new AddOddService();
    addOddService.addOdd(num);
    scanner.close();
    
  }
}