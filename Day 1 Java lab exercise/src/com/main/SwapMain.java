package com.main;

import com.service.SwapService;

/**
 * 
 * Swap two variables.
 *
 */
public class SwapMain {

	public static void main(String[] args) {
		SwapService swapService = new SwapService();
		swapService.swap(100, 200);
	}

}
