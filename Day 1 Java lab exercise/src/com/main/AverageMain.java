package com.main;

import com.service.Average;

/**
 * 
 * Print average of three numbers.
 *
 */
public class AverageMain {

	public static void main(String[] args) {
		int arr[] = {10, 20, 30};
		Average ave = new Average();
		System.out.println(ave.average(arr));

	}

}
