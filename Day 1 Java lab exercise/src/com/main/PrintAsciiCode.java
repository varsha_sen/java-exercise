package com.main;
/**
 * 
 * Print Ascii value.
 *
 */
public class PrintAsciiCode {
	public static void main(String[] args) {
		char ch1 = 'A';
		char ch2 = 'B';
		int asciivalue1 = ch1; // Ascii value
		int asciivalue2 = ch2; // Ascii value
		System.out.println("The ASCII value of " + ch1 + " is: " + asciivalue1);
		System.out.println("The ASCII value of " + ch2 + " is: " + asciivalue2);
	}
}