package com.main;

import java.util.Scanner;

import com.model.Calculator;
import com.service.CalculatorService;

/**
 * 
 * Calculator Program
 *
 */
public class CalculatorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter value of num1 : ");
		int num1 = scanner.nextInt();
		System.out.println("Enter value of num2 : ");
		int num2 = scanner.nextInt();
		Calculator calculate = new Calculator(num1, num2);
		CalculatorService cService = new CalculatorService();
		System.out.println(cService.add(calculate.getNum1(), calculate.getNum2()));
		System.out.println(cService.sub(calculate.getNum1(), calculate.getNum2()));
		System.out.println(cService.multi(calculate.getNum1(), calculate.getNum2()));
		System.out.println(cService.div(calculate.getNum1(), calculate.getNum2()));

		scanner.close();

	}

}
