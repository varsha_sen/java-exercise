package com.main;

import com.service.FizzBuzzService;

public class FizzBuzzMain {
	public static void main(String[] args) {
		int arr[] = new int[101];
		int i;
		for(i = 1; i<= 100; i++) {
			arr[i] = i;
		}
		FizzBuzzService fizzBuzzService = new FizzBuzzService();
		fizzBuzzService.fizzBuzz(arr);

	}
}