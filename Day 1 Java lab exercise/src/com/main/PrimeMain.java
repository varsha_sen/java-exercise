package com.main;
/**
 * Check a number is prime number or not.
 */
import java.util.Scanner;

import com.service.PrimeService;

public class PrimeMain {
	public static void main(String args[]) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter any number:");
		int num = scan.nextInt();
		PrimeService pService = new PrimeService();
		pService.checkPrime(num);
		scan.close();
		
	}
}