package com.main;
/**
 * 
 * 
 * Print the result of the following operations.
 *
 */
public class Calculate {

	public static void main(String[] args) {
		int a = -5 + 8 *  6;
		float b = (55+9) % 9;
		float c = 20 + -3*5 / 8;
		float d = 5 + 15 / 3 * 2 - 8 % 3;
		
		System.out.println("Output of a : " +a);
		System.out.println("Output of b : " +b);
		System.out.println("Output of c : " +c);
		System.out.println("Output of d : " +d);

	}

}
