package com.main;

import java.util.Scanner;

import com.service.UserMainCode;

/**
 * 
 * Sum of squares of even digits (values) present in the given number.
 *
 */
public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a positive number : ");
		int number = scanner.nextInt();
		System.out.println(UserMainCode.sumOfSquaresOfEvenDigits(number));

		scanner.close();

	}

}
