package com.service;

public class FizzBuzzService {
	public void fizzBuzz(int arr[]) {
		for (int i = 0; i <= 100; i++) {
			if (arr[i] % 3 == 0 && arr[i] % 5 == 0) {
				System.out.printf("\n%d: fizz buzz", arr[i]);
			} else if (arr[i] % 5 == 0) {
				System.out.printf("\n%d: buzz", arr[i]);
			} else if (arr[i] % 3 == 0) {
				System.out.printf("\n%d: fizz", arr[i]);
			}
		}
		System.out.printf("\n");
	}

}
