package com.service;

public class SwapService {
	public void swap(int x, int y) {
		System.out.println("Before Swap");
		System.out.println("x = " + x);
		System.out.println("y = " + y);

		int temp = x;
		x = y;
		y = temp;

		System.out.println("After swap");
		System.out.println("x = " + x);
		System.out.println("y = " + y);
	}

}
