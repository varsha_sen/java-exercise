package com.service;

public class AddOddService {
	public void addOdd(int num) {
		int summ=0;
	    while(num>0){
	      int remm = num%10;
	      if(remm%2!=0){
	        summ = summ+remm;
	      }
	      num = num/10;
	    }
	    
	    if(summ%2==0){
	      System.out.println("Sum of odd digits is even");
	    }else{
	      System.out.println("Sum of odd digits is odd");
	    }
	}

}
