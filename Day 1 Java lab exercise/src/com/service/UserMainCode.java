package com.service;

public class UserMainCode {
	public static int sumOfSquaresOfEvenDigits(int num) {
		int n1 = 0;
		int sum = 0;
	    while(num!=0)
	    {
	    n1=num%10;
	    if((n1%2)==0)
	    sum += n1 * n1;
	    num/=10;
	    }
	   return sum;
	}

}
